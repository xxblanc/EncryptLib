package com.xxlib.encryptlib;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tv = (TextView) findViewById(R.id.sample_text);
        String data = "000000010963100000000120161223110518";
        String result = callEncryptJni(data);
        tv.setText(result);
    }


    private String callEncryptJni(String data){
        String result = new Encrypt().encrypt(data);


        return result;
    }

}
