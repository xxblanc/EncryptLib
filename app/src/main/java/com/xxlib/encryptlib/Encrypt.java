package com.xxlib.encryptlib;

import android.text.TextUtils;
import android.util.Log;

import java.util.Arrays;

public class Encrypt {

    static {
//        System.loadLibrary("xxlib");
        System.loadLibrary("xxlibcpp");
    }

//    public String encrypt(String data){
//        int[] indata = getUnsignedBytes(data);
//        int[] outdata = new int[8];
//        int[] ret = new int[1];
//        CBC(indata.length, indata, outdata, ret);
//        if(ret[0] != 0x00){
//            return "加密失败";
//        } else {
//
//            return Arrays.toString(outdata);
//        }
//    }
//
//    private int[] getUnsignedBytes(String data){
//        int[] result = new int[data.length()/2];
//        for(int i =0; i < data.length()/2; i++){
//            int value = Byte.valueOf(data.substring(i*2, i*2+2));
//            result[i] = value;
//        }
//        Log.e("====", "getUnsignedBytes: " +Arrays.toString(result));
//        return result;
//    }
//
//    public native void CBC(int len, int[] indata, int[] outdata, int[] ret);

    private byte[] getHexBytes(String data){
        byte[] result = new byte[data.length()/2];
        for(int i =0; i < data.length()/2; i++){
            byte value = Byte.valueOf(data.substring(i*2, i*2+2), 16);
            result[i] = value;
        }
        Log.e("====", "getUnsignedBytes: " +Arrays.toString(result));
        return result;
    }

//    public String encrypt(String data){
//        int len = data.length();
//        char[] indata = data.toCharArray();
//        char[] outdata = new char[8];
//        char[] ret = new char[1];
//        CBC(len, indata, outdata, ret);
//        if(ret[0] != 0x00){
//            return "加密失败";
//        } else {
//
//            return Arrays.toString(outdata);
//        }
//    }

    public String encrypt(String data){
        byte[] indata = getHexBytes(data);
        byte[] outdata = new byte[8];
        byte[] ret = new byte[1];
        CBCcpp(indata.length, indata, outdata, ret);
        if(ret[0] != 0x00){
            return "加密失败";
        } else {
            return Arrays.toString(outdata);
        }

//        String indata_ = new String(indata);
//        String outdata = "";
//        String ret = "";
//        CBCcpp(indata.length, indata_, outdata, ret);
//        return outdata;

    }

//    private native String CBC2(int length, String s, String outdata, String ret);


//    public native void CBC(int len, byte[] indata, byte[] outdata, byte[] ret);

    public native void CBCcpp(int len, byte[] indata, byte[] outdata, byte[] ret);

//    public native void CBCcpp(int len, String indata, String outdata, String ret);

}
