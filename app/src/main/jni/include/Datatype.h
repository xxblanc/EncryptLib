#ifndef __DATATYPE_H
#define __DATATYPE_H

#define SYS_64BIT	0
#define SYS_32BIT	1

 
#if SYS_64BIT 
typedef  unsigned char	*	STRPTR;
typedef  int			INT4; 
typedef  unsigned int      	UINT4; 
typedef  int	*		INT4PTR;
typedef  unsigned int*		UINT4PTR;

typedef	 unsigned short		US;
typedef	 unsigned char		UC; 
#endif

#if SYS_32BIT
typedef  unsigned char	*	STRPTR;
typedef  long int			INT4; 
typedef  unsigned long int      	UINT4; 
typedef  long int	*		INT4PTR;
typedef  unsigned long int*		UINT4PTR;

typedef	 unsigned short		US;
typedef	 unsigned char		UC; 
#endif


#endif
