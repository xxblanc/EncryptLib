#include "stdio.h"

#include "string.h"
#include "memory.h"

//#pragma pack(1)
// #include "SJL10A.h"
#include "Datatype.h"

#define MAX 2048
#define SJL10PROTO  _declspec(dllexport) void _stdcall 

////////////////////////////////////////////////////////////////////////////////////
static void SingleDES(unsigned char *Fout, unsigned char *In,const unsigned char *key,int datalen, unsigned char type); 
static void TripleDES(unsigned char *Fout,unsigned char *In,const unsigned char *key,int datalen, unsigned char type);
static void F_func(unsigned char In[32], const unsigned char Ki[48]);
static void S_func(unsigned char Out[32], const unsigned char In[48]);
static void Transform(unsigned char *Out, const unsigned char *In, const unsigned char *Table, int len);
static void Xor(unsigned char *InA, const unsigned char *InB, int len);
static void SetSubKey(unsigned char Key_box[16][48] ,const unsigned char Key[8]);
static void RotateL(unsigned char *In, int len, int loop);
static void BitToByte(unsigned char *Out, const unsigned char *In, int Bits);
static void ByteToBit(unsigned char *Out,const unsigned char *In,int len);
static void DKey(unsigned char *Out,unsigned char *key,const unsigned char *data); 
static void CData(unsigned char *Out,unsigned char *In);
static void DES(unsigned char *out, const unsigned char *In,const unsigned char *key);
static void RDES(unsigned char *out,const unsigned char *in,const unsigned char *key);
//static void TripleMAC(unsigned char * SingleMACKey,unsigned char *InitData,unsigned int dataLen,unsigned char *In,unsigned char *MACData);		
static void SingleMAC(unsigned char * SingleMACKey,unsigned char *InitData,unsigned int dataLen,unsigned char *In,unsigned char *MACData); 
static UINT4 LenRet(UINT4 input);
static UINT4 PlainLenRet(UINT4 inputLen, const unsigned char *inputData);

///////////////////////////////////////////////////////////////////////////////////
const static unsigned char Key[16]={0x84,0x5E,0x3F,0x65,0x7B,0xD1,0xE9,0x7A,0x08,0xDB,0xCE,0x6A,0x5E,0x7F,0xB0,0x4C};
const static unsigned char InitData[8] = {0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00};

/*SJL10PROTO Data_Encrypt(  
	  UINT4 Key_Index
	, const unsigned char *div_factor
	, UINT4 Plain_Data_Len
	, unsigned char *Plain_Data
	, UINT4 *Encrypted_Data_Len
	, unsigned char *Encrypted_Data
	, unsigned char *ret)
{   
	int i;
	unsigned char temp[16];
	unsigned char tempkey[16];
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(i=0;i<16;i++)
		{
			temp[i]=AllKey[Key_Index][i];
		}
		ret[0]=0x00;
	}
	else
	{
		ret[0]=0x71;
	}

	DKey(tempkey,temp,div_factor);

	TripleDES(Encrypted_Data,Plain_Data,tempkey,Plain_Data_Len,1);
	Encrypted_Data_Len[0]=LenRet(Plain_Data_Len);
}


SJL10PROTO Data_Decrypt(  
	  UINT4 Key_Index
	, const unsigned char *div_factor
	, UINT4 Encrypted_Data_Len
	, unsigned char *Encrypted_Data
	, UINT4 *Plain_Data_Len
	, unsigned char *Plain_Data
	, unsigned char *ret)
{
	unsigned char temp[16];
	unsigned char tempkey[16];
	UINT4 flag;

	int i;
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(i=0;i<16;i++)
		{
			temp[i]=AllKey[Key_Index][i];
		}
		ret[0]=0x00;
	}
	else 
	{
		ret[0]=0x71;
	}

	DKey(tempkey,temp,div_factor);
	TripleDES(Plain_Data,Encrypted_Data,tempkey,Encrypted_Data_Len,0); 

	flag = PlainLenRet(Encrypted_Data_Len, Plain_Data);
	if(flag == -1)
		ret[0] = 0x92;
	else
	{
		Plain_Data_Len[0] = flag;
	}	
}

SJL10PROTO MAC_Generate(  
	  UINT4 Key_Index
	, UINT4 div_flag
	, const unsigned char *div_factor
	, unsigned char *SessionKey_Seed
	, UINT4 MAC1_Data_Len
	, unsigned char *MAC1_Data
	, unsigned char *MAC1
	, unsigned char *ret)
{
	unsigned char MAC_0[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	unsigned char MAC_temp[8];
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char tempkey_1[16];
	int y,i;
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(y=0;y<16;y++)
		{
			temp[y]=AllKey[Key_Index][y];
		}

		DKey(tempkey,temp,div_factor);
		TripleDES(tempkey_1,SessionKey_Seed,tempkey,8,1);
		SingleMAC(tempkey_1,MAC_0,MAC1_Data_Len,MAC1_Data,MAC_temp);
		for(i=0;i<4;i++)
		{
			MAC1[i]=MAC_temp[i];
		}

        ret[0]=0x00; 
	}
	else 
	{
		ret[0]=0x71; 
	}
}

SJL10PROTO MAC_Verify(
	  UINT4 Key_Index
	, UINT4 div_flag
	, const unsigned char *div_factor
	, unsigned char *SessionKey_Seed
	, UINT4 MAC_Data_Len
    , unsigned char *MAC_Data
    , unsigned char *MAC
    , unsigned char *ret)
{    
	int t=0;
	unsigned char MAC_0[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	unsigned char MAC1[8];
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char tempkey_1[16];
	int j,i;
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(j=0;j<16;j++)
		{
			temp[j]=AllKey[Key_Index][j];
		}

		DKey(tempkey,temp,div_factor);

		TripleDES(tempkey_1,SessionKey_Seed,tempkey,8,1);

		SingleMAC(tempkey_1,MAC_0,MAC_Data_Len,MAC_Data,MAC1);

		for(i=0;i<4;i++)
		{
			if(MAC[i]==MAC1[i])
				t++;
			else
				break;
		}
		if(t==4)
			ret[0]=0x00;
		else
			ret[0]=0x21;
	}
	else
	{
		ret[0]=0x71;
	}
}

SJL10PROTO MAC_VerifyAndGenerate(
	  UINT4 Key_Index
	, UINT4 div_flag
	, const unsigned char *div_factor
	, unsigned char *SessionKey_Seed
	, UINT4 MAC1_Data_Len
	, unsigned char *MAC1_Data
	, unsigned char *MAC1
	, UINT4 MAC2_Data_Len
	, unsigned char *MAC2_Data
	, unsigned char *MAC2
	, unsigned char *ret)
{
	int t=0;
	unsigned char MAC_0[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	unsigned char MAC[8];
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char tempkey_1[16];
	int w,i;
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(w=0;w<16;w++)
		{
		   temp[w]=AllKey[Key_Index][w];
		}

		DKey(tempkey,temp,div_factor);
		TripleDES(tempkey_1,SessionKey_Seed,tempkey,8,1);
		SingleMAC(tempkey_1,MAC_0,MAC1_Data_Len,MAC1_Data,MAC);

		for(i=0;i<4;i++)
		{
			if(MAC1[i]==MAC[i])
				t++;
			else
				break;
		}

		if(t==4)
		{
			ret[0]=0x00;
			SingleMAC(tempkey_1,MAC_0,MAC2_Data_Len,MAC2_Data,MAC2);
		}
		else
		{
			ret[0]=0x21;
		}
	}
	else
	{
		ret[0]=0x71;
	}
}


SJL10PROTO TAC_Generate(
	  UINT4 Key_Index
	, UINT4 div_flag
	, const unsigned char *div_factor
	, UINT4 TAC_Data_Len
	, unsigned char *TAC_Data
	, unsigned char *TAC
	, unsigned char *ret)
{
	unsigned char TAC_0[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char tk1[128];
	unsigned char tk2[128];
	unsigned char KL[16];
	unsigned char KR[16];
	int w,i;
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(w=0;w<16;w++)
		{
			temp[w]=AllKey[Key_Index][w];
		}

		DKey(tempkey,temp,div_factor);
		for(i=0;i<8;i++)
		{
			KL[i]=tempkey[i];
			KR[i]=tempkey[8+i];
		}

		ByteToBit(tk1,KL,64);
		ByteToBit(tk2,KR,64);
		Xor(tk1,tk2,64);	
		BitToByte(KL,tk1,64);

		SingleMAC(KL,TAC_0,TAC_Data_Len,TAC_Data,TAC);
       
		ret[0]=0x00;
	}
	else
	{
		ret[0]=0x71;
	}
}


SJL10PROTO TAC_Verify(
      UINT4 Key_Index
    , UINT4 div_flag
    , const unsigned char *div_factor
    , UINT4 TAC_Data_Len
    , unsigned char *TAC_Data
    , unsigned char *TAC
    , unsigned char *ret)
{      
	int t=0;
	unsigned char TAC_0[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char TAC1[16];
	unsigned char KL[16];
	unsigned char KR[16];
	unsigned char tk1[128];
	unsigned char tk2[128];
	int w,i,s;
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for( w=0;w<16;w++)
		{
			temp[w]=AllKey[Key_Index][w];
		}

		DKey(tempkey,temp,div_factor);

		for(s=0;s<8;s++)
		{
			KL[s]=tempkey[s];
			KR[s]=tempkey[8+s];
		}

		ByteToBit(tk1,KL,64);
		ByteToBit(tk2,KR,64);
		Xor(tk1,tk2,64);	
		BitToByte(KL,tk1,64);

		SingleMAC(KL,TAC_0,TAC_Data_Len,TAC_Data,TAC1);
		for(i=0;i<4;i++)
		{
			if(TAC[i]==TAC1[i])
				t++;
			else
				break;
		}
		if(t==4)
			ret[0]=0x00;
		else
			ret[0]=0x21;
	}
	else
	{
		ret[0]=0x71;
	}
}


SJL10PROTO App_MAC_Generate(
	  UINT4 Key_Index
	, const unsigned char *div_factor
	, UINT4 MAC_Data_Len
	, unsigned char *MAC_Data
	, unsigned char *MAC
	, unsigned char *ret)
{     
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char MAC_0[8];
	unsigned char MAC_1[MAX];
	UINT4 q,i,w;

	for(q=0;q<8;q++)
	{
		MAC_0[q]= MAC_Data[q]; 
	}
	for( w=0;w<MAC_Data_Len-8;w++)
	{
		MAC_1[w]= MAC_Data[w+8]; 
	}
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(i=0;i<16;i++)
		{
			temp[i]=AllKey[Key_Index][i];
		}
		DKey(tempkey,temp,div_factor);
		TripleMAC(tempkey,MAC_0,MAC_Data_Len-8,MAC_1,MAC);

		ret[0]=0x00;
	}
	else
	{
		ret[0]=0x71;
	}
}

SJL10PROTO App_MAC_Verify(
      UINT4 Key_Index
    , const unsigned char *div_factor
    , UINT4 MAC_Data_Len
    , unsigned char *MAC_Data
    , unsigned char *MAC
    , unsigned char *ret)
{     
	unsigned char temp[16];
	unsigned char tempkey[16];
	unsigned char MAC_0[8];
	unsigned char MAC_1[MAX];
	unsigned char MAC1[8];
    int t=0;
	UINT4 q,w,i;
	for(q=0;q<8;q++)
	{
		MAC_0[q]= MAC_Data[q]; 
	}
	for(w=0;w<MAC_Data_Len-8;w++)
	{
		MAC_1[w]= MAC_Data[w+8]; 
	}
	if((Key_Index>=1&&Key_Index<=15)||(Key_Index==23)) 
	{
		for(i=0;i<16;i++)
		{
			temp[i]=AllKey[Key_Index][i];
		}
		DKey(tempkey,temp,div_factor);
		TripleMAC(tempkey,MAC_0,MAC_Data_Len-8,MAC_1,MAC1);

		for(i=0;i<4;i++)
		{
			if(MAC[i]==MAC1[i])
				t++;
			else
				break;
		}
		if(t==4)
			ret[0]=0x00;
		else
			ret[0]=0x21;

	}
	else
	{
		ret[0]=0x71;
	}
}*/


void CBC(int datalen,unsigned char *In,unsigned char *Out, unsigned char *ret)
{
	int Bdatalen=datalen*8;
	unsigned char temp1[64];
	unsigned char temp2[64];
	unsigned char KL[8];
    unsigned char KR[8];
	unsigned char temp[16];
	unsigned char tempout[16];
//	unsigned int dlen=Bdatalen%64;
//	unsigned int t=Bdatalen/64;
    int dlen=Bdatalen%64;
    int t=Bdatalen/64;
	unsigned int r,i,j,u,n;
	unsigned char Inp[MAX];
	for(n=0;n<datalen;n++)
	{
		Inp[n]=In[n];
//        Inp[n]=(unsigned char)( In[n] & 0xff);
	}
	switch(dlen)
	{
	   	 case 0 : {Bdatalen=Bdatalen+64;Inp[t*8+0]=0x80;Inp[t*8+1]=0x00;Inp[t*8+2]=0x00;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 8 : {Bdatalen=Bdatalen+56;Inp[t*8+1]=0x80;Inp[t*8+2]=0x00;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 16 :{Bdatalen=Bdatalen+48;Inp[t*8+2]=0x80;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
	     case 24 :{Bdatalen=Bdatalen+40;Inp[t*8+3]=0x80;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 32 :{Bdatalen=Bdatalen+32;Inp[t*8+4]=0x80;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 40 :{Bdatalen=Bdatalen+24;Inp[t*8+5]=0x80;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 48 :{Bdatalen=Bdatalen+16;Inp[t*8+6]=0x80;Inp[t*8+7]=0x00;};break;
	 	 case 56 :{Bdatalen=Bdatalen+8;Inp[t*8+7]=0x80;};break;
	}
    t=Bdatalen/64;
	for( r=0;r<8;r++)
	{
        KL[r]=Key[r];
		KR[r]=Key[8+r];
	}

    ByteToBit(temp1,InitData,64);

    for( i=0;i<t;i++)
	{
		for( j=0;j<8;j++)
		{
			temp[j]=Inp[i*8+j];
		}
        ByteToBit(temp2,temp,64);
	    Xor(temp1,temp2,64);	
        BitToByte(temp,temp1,64);
        DES(temp1, temp,KL);
	}
	BitToByte(temp,temp1,64);
	RDES(temp1, temp,KR);
	BitToByte(temp,temp1,64);
	DES(temp1, temp,KL);
	BitToByte(tempout,temp1,64);

	for( u=0;u<4;u++)
	{
		Out[u]=tempout[u];
	}

	ret[0]=0x00;
}


///////////////////////////////////////////////////////////////////////////////////
const static unsigned char IP_Table[64] = {
	58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
	62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
	57, 49, 41, 33, 25, 17,  9, 1, 59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7
};
const static unsigned char E_Table[48] = {
	32,  1,  2,  3,  4,  5,  4,  5,  6,  7,  8,  9,
	 8,  9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25,
	24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32,  1
};
const static unsigned char P_Table[32] = {
	16, 7, 20, 21, 29, 12, 28, 17, 1,  15, 23, 26, 5,  18, 31, 10,
	2,  8, 24, 14, 32, 27, 3,  9,  19, 13, 30, 6,  22, 11, 4,  25
};
const static int S_Box[8][4][16] = {
	// S1
	14,	 4,	13,	 1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
	 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
	 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
    15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13,
	// S2
    15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
	 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
	 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
    13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9,
	// S3.............................. 
    10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
	13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
	13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
     1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12,
	// S4 
     7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
	13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
	10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
     3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14,
	// S5 
     2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
	14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
	 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
    11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3,
	// S6 
    12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
	10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
	 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
     4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13,
	// S7 
     4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
	13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
	 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
     6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12,
	// S8 
    13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
	 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
	 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
     2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11
};
const static unsigned char PC1_Table[56] = {
	57, 49, 41, 33, 25, 17,  9,  1, 58, 50, 42, 34, 26, 18,
	10,  2, 59, 51, 43, 35, 27, 19, 11,  3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,  7, 62, 54, 46, 38, 30, 22,
	14,  6, 61, 53, 45, 37, 29, 21, 13,  5, 28, 20, 12,  4
};
const static unsigned char PC2_Table[48] = {
	14, 17, 11, 24,  1,  5,  3, 28, 15,  6, 21, 10,
	23, 19, 12,  4, 26,  8, 16,  7, 27, 20, 13,  2,
	41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32
};
const static unsigned char IPR_Table[64] = {
	40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31,
	38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27,
	34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41,  9, 49, 17, 57, 25
};
const static unsigned char LOOP_Table[16] ={
	1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1
};
//////////////////////////////////////////////////////////////////////////



static void ByteToBit(unsigned char *Out, const unsigned char *In, int len)
{   
	int i,j;
    for( i=0; i<8; ++i)
	{
		for( j=0;j<(len/8);++j)
		{
            Out[(i+1)*8-(j+1)] = (In[i]>>j) & 1;
		}
	}
}

static void BitToByte(unsigned char *Out, const unsigned char *In, int Bits) 
{ 
      int i=0;
      memset(Out, 0, Bits>>3);
      for(i=0; i<Bits; ++i)
	  {
         Out[i>>3] |= In[i]<<(7 - i&7);
	  }
} 

static void F_func(unsigned char In[32], const unsigned char Ki[48])
{
    unsigned char MR[48];
    Transform(MR, In, E_Table, 48);
    Xor(MR, Ki, 48);
    S_func(In, MR);
    Transform(In, In, P_Table, 32);
}

static void Transform(unsigned char *Out, const unsigned char *In, const unsigned char *Table, int len)
{
	unsigned char Tmp[64];
	int i;
    for(i=0; i<len; ++i)
	{
		Tmp[i] = In[ Table[i]-1 ];
	}
#ifndef NDEBUG
	if(len > sizeof(Tmp))
		printf("Assertion failure: Transform len = %d, exceed the 64 bytes limit", len);
#endif
	memcpy(Out, Tmp, len);
}

static void S_func(unsigned char Out[32], const unsigned char In[48])
{
	unsigned char m=0;
    unsigned char t=0;
	unsigned char j;
	unsigned char k;
	unsigned char i;
	unsigned char p;
	int g;
    for( i=0; i<8; ++i) 
	{
		j = (In[m+0]*2) + In[m+5];
		k = (In[m+1]*8) + (In[m+2]*4) + (In[m+3]*2) + In[m+4];
		g = S_Box[i][j][k];

		for(p=0;p<4;++p)
		{
			 Out[t+p]=(g>>(3-p))&1;
		}
		t=t+4;
		m=m+6;
	}
}

static void Xor(unsigned char *InA, const unsigned char *InB, int len)
{
	int i;
    for(i=0; i<len; ++i)
	{
        InA[i] ^= InB[i];
	}
}


static void SetSubKey(unsigned char Key_box[16][48] ,const unsigned char Key[8])
{
	int j,i,k;
    unsigned char K[64],K2[64], KL[28], KR[28];
    ByteToBit(K2, Key, 64);
    Transform(K, K2, PC1_Table, 56);

	for(j=0;j<28;j++)
	{
		KL[j]=K[j];
		KR[j]=K[28+j];
	}
    
    for(i=0; i<16; ++i)
	{
       RotateL(KL, 28, LOOP_Table[i]);
       RotateL(KR, 28, LOOP_Table[i]);
       for(k=0;k<28;k++)
	   {
		   K[k]=KL[k];
		   K[28+k]=KR[k];
	   }
       Transform(Key_box[i], K, PC2_Table, 48);
    }
}

static void DES(unsigned char *out, const unsigned char *In,const unsigned char *key)
{
	unsigned char Key_box[16][48];
    unsigned char M[64], tmp[32], Li[32], Ri[32];
    int b,c,d,i;

	SetSubKey(Key_box, key);
	ByteToBit(M, In, 64);
    Transform(M, M, IP_Table, 64);

	for(b=0;b<32;b++)
	{
		Li[b]=M[b];
		Ri[b]=M[32+b];
	}

    for(c=0; c<16; c++) 
	{
        memcpy(tmp, Ri, 32);
        F_func(Ri, Key_box[c]);
        Xor(Ri, Li, 32);
        memcpy(Li, tmp, 32);		
    }

    for( d=0;d<32;d++)
	{
		M[d]=Ri[d];
		M[32+d]=Li[d];
	}

    Transform(M, M, IPR_Table, 64);
	for(i=0;i < 64;i++) 
	{
		out[i]=M[i];
	}
}

static void RDES(unsigned char *out,const unsigned char *in,const unsigned char *key)
{
	int i,g,h,j;
	unsigned char Key_box[16][48];
    unsigned char M[64], tmp[32], Li[32], Ri[32];

	SetSubKey(Key_box, key);
    ByteToBit(M, in, 64);
    Transform(M, M, IP_Table, 64);
    
	for( g=0;g<32;++g)
	{
		Li[g]=M[g];
		Ri[g]=M[32+g];
	}
	for(i=15; i>=0; i--) 
	{ 
        memcpy(tmp,Ri, 32);
        F_func(Ri, Key_box[i]);
        Xor(Ri, Li, 32);
        memcpy(Li, tmp, 32);
	}	
	for( h=0;h<32;h++)
	{
		M[h]=Ri[h];
		M[32+h]=Li[h];
	}
    Transform(M, M, IPR_Table, 64);
	for(j=0;j<64;j++)
	{
		out[j]=M[j];
	}
}

static void RotateL(unsigned char *In, int len, int loop)
{
	unsigned char Tmp[MAX];
    memcpy(Tmp, In, loop);
    memcpy(In, In+loop, len-loop);
    memcpy(In+len-loop, Tmp, loop);
}

static void string2hex(unsigned char *hex,unsigned char *str,int len)
{
    int i, j;
    for (i=0;i<len;i=i+2)
    {
//        sscanf_s((char *)&(str[i]), "%2x", &j); // by lmz 2018-05-28
        hex[i/2]=j;
    }
}

static void SingleDES(unsigned char *Fout,unsigned char *In,const unsigned char *key,int datalen,unsigned char type)
{   
	unsigned char temp[64];
	unsigned char tp[8];
	unsigned char Inp[MAX + 8];

	int Bdatalen=datalen*8;
	int dlen=Bdatalen%64;
	int k=64-dlen;
	int t=Bdatalen/64;
	int i;

	memcpy(Inp, In, datalen);    
	switch(dlen)
	{
	     case 0 :
		 case 8 : {Bdatalen=Bdatalen+56;Inp[t*8+1]=0x80;Inp[t*8+2]=0x00;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;In[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 16 :{Bdatalen=Bdatalen+48;Inp[t*8+2]=0x80;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
	     case 24 :{Bdatalen=Bdatalen+40;Inp[t*8+3]=0x80;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 32 :{Bdatalen=Bdatalen+32;Inp[t*8+4]=0x80;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 40 :{Bdatalen=Bdatalen+24;Inp[t*8+5]=0x80;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 48 :{Bdatalen=Bdatalen+16;Inp[t*8+6]=0x80;Inp[t*8+7]=0x00;};break;
	 	 case 56 :{Bdatalen=Bdatalen+8;Inp[t*8+7]=0x80;};break;
	}

   if(type==1)
   {	
		for( i=0;i<Bdatalen/64;i++)
		{
			for( k=0;k<8;k++)
		    {
				tp[k]=Inp[i*8+k];
		    }			     
		    DES(temp,tp,key);
			BitToByte(Fout + (i << 3), temp, 64);
	  }
   }
   else
   {
		for( i=0;i<Bdatalen/64;i++)
		{
			for( k=0;k<8;k++)
		    {
				tp[k]=Inp[i*8+k];
		    }		     			     
		    RDES(temp,tp,key);
			BitToByte(Fout + (i << 3), temp, 64);
	     }
	}
}

static void TripleDES(unsigned char *Fout,unsigned char *In,const unsigned char *key,int datalen, unsigned char type)
{
	unsigned char temp[64];
	unsigned char KL[8];
	unsigned char KR[8];
	unsigned char tp[8];
	unsigned char Inp[MAX + 8];

	int Bdatalen=datalen*8;
	int dlen=Bdatalen%64;
	int t=Bdatalen/64;
    int i,k;

	memcpy(Inp, In, datalen);
	switch(dlen)
	{
	     case 0 :
		 case 8 : {Bdatalen=Bdatalen+56;Inp[t*8+1]=0x80;Inp[t*8+2]=0x00;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 16 :{Bdatalen=Bdatalen+48;Inp[t*8+2]=0x80;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
	     case 24 :{Bdatalen=Bdatalen+40;Inp[t*8+3]=0x80;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 32 :{Bdatalen=Bdatalen+32;Inp[t*8+4]=0x80;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 40 :{Bdatalen=Bdatalen+24;Inp[t*8+5]=0x80;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 48 :{Bdatalen=Bdatalen+16;Inp[t*8+6]=0x80;Inp[t*8+7]=0x00;};break;
	 	 case 56 :{Bdatalen=Bdatalen+8;Inp[t*8+7]=0x80;};break;
	}
	for(i=0;i<8;i++)
	{
		KL[i]=key[i];
		KR[i]=key[8+i];
	}
//////////////////////////////////////////////
	if(type==1)
	{
		for(i=0; i<Bdatalen/64; i++)
		{
			for(k=0;k<8;k++)
		    {
				tp[k]=Inp[i*8+k];
		    }
			//
		    DES(temp,tp,KL);
            BitToByte(tp,temp,64);   
			RDES(temp,tp,KR);
            BitToByte(tp,temp,64); 
            DES(temp,tp,KL);
			//
			BitToByte(Fout + (i << 3), temp, 64);
		}
   }
   else
   {
		for(i=0; i<Bdatalen/64; i++)
		{
			for(k=0; k<8; k++)
		    {
				tp[k]=Inp[i*8+k];
		    }
			//
		    RDES(temp,tp,KL);
            BitToByte(tp,temp,64);
			DES(temp,tp,KR);
            BitToByte(tp,temp,64);
            RDES(temp,tp,KL);
			//
			BitToByte(Fout + (i << 3), temp, 64);
		}
	}
}

static void CData(unsigned char *Out,unsigned char *In)
{
	int i;
	unsigned char tp[64];
	unsigned char tpout[64];
	ByteToBit(tp,In,64);
	for(i=0;i<64;i++)
	{
		if(tp[i]==0)
			tpout[i]=1;
		else
			tpout[i]=0;
	}
	BitToByte(Out,tpout,64);
}

static void DKey(unsigned char *Out,unsigned char *key,const unsigned char *data)
{
	unsigned char LD[16];
	unsigned char RD[16];
	unsigned char RTD[16];
	unsigned char LK[16];
	unsigned char RK[16];
	int i,k;
	for(i=0;i<8;i++)
	{
		LD[i]=data[i];
		RTD[i]=data[i];
	}
	CData(RD,RTD);

	TripleDES(LK,LD,key,8,1);
	TripleDES(RK,RD,key,8,1);
	for(k=0; k<8; k++)
	{
		Out[k]=LK[k];
		Out[8+k]=RK[k];
	}
}

static void SingleMAC(unsigned char *Key,unsigned char *InitData,unsigned int datalen,unsigned char *In,unsigned char *MACData)
{
	int Bdatalen=datalen*8;
    unsigned char temp1[64];
	unsigned char temp2[64];
	unsigned char temp[8];
	int dlen=Bdatalen%64;
	int t=Bdatalen/64;
	int i,j;
	unsigned int w,n;
	unsigned char Inp[MAX + 8];
	for(n=0;n<datalen;n++)
	{
		Inp[n]=In[n];
	}
    
	switch(dlen)
	{
         case 0 : {Bdatalen=Bdatalen+64;Inp[t*8+0]=0x80;Inp[t*8+1]=0x00;Inp[t*8+2]=0x00;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 8 : {Bdatalen=Bdatalen+56;Inp[t*8+1]=0x80;Inp[t*8+2]=0x00;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 16 :{Bdatalen=Bdatalen+48;Inp[t*8+2]=0x80;Inp[t*8+3]=0x00;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
	     case 24 :{Bdatalen=Bdatalen+40;Inp[t*8+3]=0x80;Inp[t*8+4]=0x00;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 32 :{Bdatalen=Bdatalen+32;Inp[t*8+4]=0x80;Inp[t*8+5]=0x00;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 40 :{Bdatalen=Bdatalen+24;Inp[t*8+5]=0x80;Inp[t*8+6]=0x00;Inp[t*8+7]=0x00;};break;
		 case 48 :{Bdatalen=Bdatalen+16;Inp[t*8+6]=0x80;Inp[t*8+7]=0x00;};break;
	 	 case 56 :{Bdatalen=Bdatalen+8;Inp[t*8+7]=0x80;};break;
	}
	
	t=Bdatalen/64;
	ByteToBit(temp1,InitData,64);

    for(i=0;i<t;i++)
	{
		for(j=0;j<8;j++)
		{
			temp[j]=Inp[i*8+j];
		}
        ByteToBit(temp2,temp,64);
	    Xor(temp1,temp2,64);
        BitToByte(temp,temp1,64);
        DES(temp1,temp,Key);
	}
    BitToByte(temp,temp1,64);
	for(w=0;w<4;w++)
	{
		MACData[w]=temp[w];
	}
}



static UINT4 LenRet(UINT4 input)
{
	int t;
	UINT4 dataLen;

	t = input % 8;
	if(t == 0)
		dataLen = input;
	else dataLen = input - t + 8;

	return dataLen;
}

static UINT4 PlainLenRet(UINT4 inputLen, const unsigned char *inputData)
{
	int t, i;
	UINT4 dataLen;

	t = inputLen / 8;
		for(i = inputLen - 8; i < inputLen; i++)
		{
			if(i == inputLen - 1)
			{
				if(inputData[i] == 0x80)
				{
				    dataLen = inputLen - 1;
				}
				else
				{
					return inputLen;
				}
			}
			else if(inputData[i] == 0x80 && inputData[i + 1] == 0x00)
			{
				dataLen = i;
				break;
			}
		}	

	return dataLen;
}

