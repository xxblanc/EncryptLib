#include <jni.h>
#include <string>

#include "../jni/Encrypt.c"

extern "C"
JNIEXPORT void
JNICALL
Java_com_xxlib_encryptlib_Encrypt_CBCcpp(JNIEnv *env, jobject /* this */, jint length,
                                         jbyteArray indata_,
                                         jbyteArray outdata_,
                                         jbyteArray ret_) {
    jbyte *indata = env->GetByteArrayElements(indata_ , NULL);

    CBC(length, (unsigned char *)indata, (unsigned char *)outdata_, (unsigned char *)ret_);

}


//extern "C"
//JNIEXPORT void
//JNICALL
//Java_com_xxlib_encryptlib_Encrypt_CBCcpp(JNIEnv *env, jobject /* this */, jint length,
//                                         jstring indata_,
//                                         jstring outdata_,
//                                         jstring ret_) {
//    unsigned char *indata = (unsigned char *) env->GetStringUTFChars(indata_ , NULL);
//
//    CBC(length, indata, (unsigned char *)outdata_, (unsigned char *)ret_);
//
//}

